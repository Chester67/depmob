package mg.gasstubs.oatrinona.presentation.fragment.task

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import mg.gasstubs.oatrinona.R
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EFragment

@EFragment(R.layout.fragment_task)
open class TaskFragment : Fragment() {

    @AfterViews
    fun initialize() {

    }

    @Click(R.id.fab_add_task)
    fun addTask(){
        val wizardFrag : Fragment = TaskWizardFragment_()
        loadFragment(wizardFrag)
        Log.i("TEST", "CLICK ADD TASK")
    }

    private fun loadFragment(fragment: Fragment) {
        // create a FragmentManager
        val fm = childFragmentManager
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        val fragmentTransaction = fm.beginTransaction()
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayoutTask, fragment)
        fragmentTransaction.commit() // save the changes
    }

}