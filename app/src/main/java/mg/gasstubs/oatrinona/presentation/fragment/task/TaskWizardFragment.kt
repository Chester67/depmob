package mg.gasstubs.oatrinona.presentation.fragment.task

import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import mg.gasstubs.oatrinona.R
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById

@EFragment(R.layout.fragment_task_wizard)
open class TaskWizardFragment : Fragment(){

    @ViewById(R.id.view_pager)
    lateinit var viewPager: ViewPager

    @AfterViews
    fun initialize() {
        if (viewPager != null) {
            val adapter =
                TaskPagerAdapter(childFragmentManager)
            viewPager.adapter = adapter
        }

    }

}