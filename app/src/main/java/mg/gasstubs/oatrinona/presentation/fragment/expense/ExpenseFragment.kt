package mg.gasstubs.oatrinona.presentation.fragment.expense

import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.diegodobelo.expandingview.ExpandingItem
import com.diegodobelo.expandingview.ExpandingList
import mg.gasstubs.oatrinona.R
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById

@EFragment(R.layout.fragment_expense)
open class ExpenseFragment : Fragment() {

    @ViewById(R.id.expanding_list_main)
    lateinit var mExpandingList: ExpandingList

    @AfterViews
    fun initialize() {
        createItems()
    }

    private fun createItems() {
        addItem(
            "John",
            arrayOf("House", "Boat", "Candy", "Collection", "Sport", "Ball", "Head"),
            R.color.guillotine_background,
            R.drawable.ic_ghost
        )
        addItem("Mary", arrayOf("Dog", "Horse", "Boat"), R.color.guillotine_background, R.drawable.ic_ghost)
        addItem("Ana", arrayOf("Cat"), R.color.guillotine_background, R.drawable.ic_ghost)
        addItem("Peter", arrayOf("Parrot", "Elephant", "Coffee"), R.color.guillotine_background, R.drawable.ic_ghost)
        addItem("Joseph", arrayOf(), R.color.guillotine_background, R.drawable.ic_ghost)
    }

    private fun addItem(title: String, subItems: Array<String>, colorRes: Int, iconRes: Int) {
        //Let's create an item with R.layout.expanding_layout
        val item = mExpandingList!!.createNewItem(R.layout.layout_expense)

        //If item creation is successful, let's configure it
        if (item != null) {
            item.setIndicatorColorRes(colorRes)
            item.setIndicatorIconRes(iconRes)
            //It is possible to get any view inside the inflated layout. Let's set the text in the item
            (item.findViewById<View>(R.id.title) as TextView).text = title

            //We can create items in batch.
            item.createSubItems(subItems.size)
            for (i in 0 until item.subItemsCount) {
                //Let's get the created sub item by its index
                val view = item.getSubItemView(i)

                //Let's set some values in
                configureSubItem(item, view, subItems[i])
            }
            item.findViewById<View>(R.id.add_more_sub_items).setOnClickListener {
                showInsertDialog(object : ExpenseFragment.OnItemCreated {
                    override fun itemCreated(title: String) {
                        val newSubItem = item.createSubItem()
                        configureSubItem(item, newSubItem!!, title)
                    }
                })
            }

            item.findViewById<View>(R.id.remove_item).setOnClickListener { mExpandingList!!.removeItem(item) }

        }
    }

    private fun configureSubItem(item: ExpandingItem, view: View, subTitle: String) {
        (view.findViewById<View>(R.id.sub_title) as TextView).text = subTitle
        view.findViewById<View>(R.id.remove_sub_item).setOnClickListener { item!!.removeSubItem(view) }
    }

    private fun showInsertDialog(positive: OnItemCreated) {
        val text = EditText(activity)
        val builder = context?.let { AlertDialog.Builder(it) }
        builder?.setView(text)
        builder?.setTitle("TITLE HERE")
        builder?.setPositiveButton(android.R.string.ok) { dialog, which -> positive.itemCreated(text.text.toString()) }
        builder?.setNegativeButton(android.R.string.cancel, null)
        builder?.show()
    }

    internal interface OnItemCreated {
        fun itemCreated(title: String)
    }
}