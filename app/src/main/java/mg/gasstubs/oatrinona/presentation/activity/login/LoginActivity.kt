package mg.gasstubs.oatrinona.presentation.activity.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.layout_login.*
import mg.gasstubs.oatrinona.R
import mg.gasstubs.oatrinona.presentation.activity.home.HomeActivity_

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        cirLoginButton.setOnClickListener {
            val intent = Intent(this, HomeActivity_::class.java)
            startActivity(intent)
        }
    }
}
