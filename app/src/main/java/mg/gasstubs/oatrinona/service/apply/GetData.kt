package mg.gasstubs.oatrinona.service.apply
import io.reactivex.Observable
import mg.gasstubs.oatrinona.data.ws.RetroCrypto
import retrofit2.http.GET


interface GetData {

    @GET("prices?key=8f5597bfefe4d840f9d442033bb88422")
    fun getData() : Observable<List<RetroCrypto>>

}
