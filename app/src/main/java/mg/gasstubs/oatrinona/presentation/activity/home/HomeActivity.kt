package mg.gasstubs.oatrinona.presentation.activity.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.yalantis.guillotine.animation.GuillotineAnimation
import butterknife.ButterKnife
import mg.gasstubs.oatrinona.R
import mg.gasstubs.oatrinona.presentation.fragment.expense.ExpenseFragment_
import mg.gasstubs.oatrinona.presentation.fragment.profil.ProfilFragment
import mg.gasstubs.oatrinona.presentation.fragment.task.TaskFragment_
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.ViewById

@EActivity(R.layout.activity_home)
open class HomeActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar

    @ViewById(R.id.root)
    lateinit var root: FrameLayout

    @ViewById(R.id.content_hamburger)
    lateinit var contentHamburger: View

    @ViewById(R.id.home_toolbar_title)
    lateinit var homeToolbarTitle: TextView

    lateinit var aniMenu : GuillotineAnimation

    @AfterViews
    fun initialize() {

        ButterKnife.bind(this)

        toolbar = findViewById<View>(R.id.toolbar) as android.support.v7.widget.Toolbar
        setSupportActionBar(toolbar)

        val guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null)
        aniMenu = GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
            .setStartDelay(RIPPLE_DURATION)
            .setActionBarViewForAnimation(toolbar)
            .setClosedOnStart(true)
            .build()
        val menuProfil = guillotineMenu.findViewById<View>(R.id.profile_group)
        menuProfil.setOnClickListener {
            val profilFrag : Fragment = ProfilFragment()
            loadFragment(profilFrag)
            aniMenu.close()
            homeToolbarTitle.text = getResources().getString(R.string.profile)
        }
        val menuDepense = guillotineMenu.findViewById<View>(R.id.feed_group)
        menuDepense.setOnClickListener {
            val depenseFrag : Fragment = ExpenseFragment_()
            loadFragment(depenseFrag)
            aniMenu.close()
            homeToolbarTitle.text = getResources().getString(R.string.profile)
        }
        val menuTache = guillotineMenu.findViewById<View>(R.id.activity_group)
        menuTache.setOnClickListener {
            val tacheFrag : Fragment = TaskFragment_()
            loadFragment(tacheFrag)
            aniMenu.close()
            homeToolbarTitle.text = getResources().getString(R.string.profile)
        }
        root.addView(guillotineMenu)
    }

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_home)
        ButterKnife.bind(this)

        toolbar = findViewById<View>(R.id.toolbar) as android.support.v7.widget.Toolbar
        setSupportActionBar(toolbar)

        val guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null)
        aniMenu = GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
            .setStartDelay(RIPPLE_DURATION)
            .setActionBarViewForAnimation(toolbar)
            .setClosedOnStart(true)
            .build()
        val menuProfil = guillotineMenu.findViewById<View>(R.id.profile_group)
        menuProfil.setOnClickListener {
            val profilFrag : Fragment = ProfilFragment()
            loadFragment(profilFrag)
            aniMenu.close()
            homeToolbarTitle.text = getResources().getString(R.string.profile)
        }
        root.addView(guillotineMenu)


    }*/

    private fun loadFragment(fragment: Fragment) {
        // create a FragmentManager
        val fm = supportFragmentManager
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        val fragmentTransaction = fm.beginTransaction()
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment)
        fragmentTransaction.commit() // save the changes
    }

    companion object {

        private val RIPPLE_DURATION: Long = 250
    }

}
