package mg.gasstubs.oatrinona.presentation.activity.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mg.gasstubs.oatrinona.R
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity

@EActivity(R.layout.activity_main)
open class MainActivity : AppCompatActivity() {

    @AfterViews
    fun initialize() {


    }
}
