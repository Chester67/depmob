package mg.gasstubs.oatrinona.presentation.fragment.task

import android.support.v4.app.Fragment
import mg.gasstubs.oatrinona.R
import org.androidannotations.annotations.EFragment

@EFragment(R.layout.fragment_task_second)
open class TaskSecondFragment : Fragment(){
}