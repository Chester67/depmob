package mg.gasstubs.oatrinona.service.businessdelegate

import mg.gasstubs.oatrinona.service.apply.GetData
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BusinessDelegate {

    fun getRequestInterface() {
        val baseUrl = "https://api.nomics.com/v1/"
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(GetData::class.java)
    }

}